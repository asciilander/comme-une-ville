      <div class="carto">
        <svg version="1.1" id="carto" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
           viewBox="0 0 800 390" preserveAspectRatio="xMidYMid meet" xml:space="preserve">
          <filter id="blurMe">
            <feGaussianBlur in="SourceGraphic" stdDeviation="1" />
          </filter>
          <rect id="carto__background" class="carto__background" width="100%" height="100%"/>
          <g id="carto__pattern">
            <defs>
              <pattern id="hero-pattern" x="0" y="0" width="600" height="600" patternUnits="userSpaceOnUse" >
                <?php
                  require get_template_directory() . '/inc/carto__hero-pattern.php';
                ?>
              </pattern>
            </defs>
            <rect style="fill: url(#hero-pattern)" x="0" y="0" width="100%" height="100%"/>
          </g>
          <g id="carto__elements">
            <defs>
              <symbol id="carto__point" viewBox="0 0 83 83">
                <rect clip-path="circle(50%) view-box" width="100%" height="100%" fill="black" opacity="0" /> 
                <path  d="M77.33,58.42l-3.29,5.65c-1.8,3.13-5.86,4.21-8.99,2.41l-13.67-7.86V74.4c0,3.6-2.98,6.58-6.58,6.58h-6.58
                  c-3.6,0-6.58-2.98-6.58-6.58V58.62l-13.67,7.86c-3.14,1.8-7.2,0.72-8.99-2.41l-3.29-5.65c-1.8-3.13-0.72-7.2,2.42-8.99l13.67-7.91
                  L8.1,33.6c-3.14-1.8-4.21-5.86-2.42-8.99l3.29-5.65c1.8-3.14,5.86-4.21,8.99-2.42l13.67,7.86V8.62c0-3.6,2.98-6.58,6.58-6.58h6.58
                  c3.6,0,6.58,2.98,6.58,6.58V24.4l13.67-7.86c3.14-1.8,7.2-0.72,8.99,2.42l3.29,5.65c1.8,3.14,0.72,7.2-2.42,8.99l-13.67,7.91
                  l13.67,7.91C78.05,51.22,79.13,55.29,77.33,58.42z"/>
              </symbol>
            </defs>
            <use xlink:href="#carto__point" id="carto__point--0" class="carto__point" x="100" y="150" width="15%" height="15%" 
                 data-categories="6"/>
            <use xlink:href="#carto__point" id="carto__point--1" class="carto__point" x="500" y="200" width="15%" height="15%"
                 data-categories="8"/>
            <use xlink:href="#carto__point" id="carto__point--2" class="carto__point" x="700" y="50" width="15%" height="15%"
                 data-categories="6"/>
            <use xlink:href="#carto__point" id="carto__point--3" class="carto__point" x="300" y="100" width="15%" height="15%"
                 data-categories="7"/>
            <use xlink:href="#carto__point" id="carto__point--4" class="carto__point" x="200" y="300" width="15%" height="15%"
                 data-categories="7"/>
            <text id="tooltip--0" class="carto__tooltip" visibility="hidden" x="200" y="190">Jardin Haguette</text>
            <text id="tooltip--1" class="carto__tooltip" visibility="hidden" x="265" y="250">Langue soninké</text>
            <text id="tooltip--2" class="carto__tooltip" visibility="hidden" x="600" y="90">Le "110"</text>
            <text id="tooltip--3" class="carto__tooltip" visibility="hidden" x="400" y="140">Atelier couture</text>
            <text id="tooltip--4" class="carto__tooltip" visibility="hidden" x="300" y="340">L'entre-pôts</text>
          </g>
        </svg>
      </div>
