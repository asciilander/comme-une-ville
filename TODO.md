
TODO
====


CARTO : NEXT
------------

- sur page dispositif et disposition, afficher sur la carte les points correspondants ?

### Scénarios

1. Survol d'un item du menu dispositif:

- affichage du tooltip correspondant sur la carte

2. Survol d'un item du menu dispositions:

- affichage des tooltips des points correpondants sur la carte
- flou / fase sur les autres points

3. Page dispositif

idem 1.

4. Page dispositions

idem 2.


TECH
----

- replacer les styles initiaux par une base vierge et tachyons
  (voir mon theme tachyons-commeuneville pour les fonctions etc…)
- utiliser [postcss-includes](https://github.com/interactivethings/postcss-includes)
  pour permettre l'injection des classes Thalyons dans mes classes.
    - Préférer [postcss-extend](https://github.com/travco/postcss-extend)





- voir pour utiliser [postscsss-modules](https://github.com/css-modules/postcss-modules)
  avec [posthtml-css-modules](https://github.com/css-modules/postcss-modules):
  pour modulariser, mais surtout effectuer des includes des classes Thalyons


Ressources
----------



Done
----


- utiliser [postcss-url](https://github.com/postcss/postcss-url) pour les assets css
- intégrer mon starter kit static avec postcss, browsersync etc.
