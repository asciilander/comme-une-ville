
    (function() {
      var carto = {
        svg: document.getElementById('carto'),
        heroPattern: document.getElementById('carto__pattern'),
        tooltips: document.getElementsByClassName('carto__tooltip'),
        points: document.getElementsByClassName('carto__point'),
        menuDispositifs: document.getElementsByClassName('nav__link--dispositifs'),
        menuDispositions: document.getElementsByClassName('nav__link--dispositions'),
        showPoint: function(point){
          point.tooltip.setAttributeNS(null, "visibility", "visible");
          point.classList.add('carto__point--hover');
          carto.heroPattern.setAttributeNS(null, "filter", "url(#blurMe)");
        },
        hidePoint: function(point){
          point.classList.remove('carto__point--hover');
          point.tooltip.setAttributeNS(null, "visibility", "hidden");
          carto.heroPattern.removeAttributeNS(null, "filter");
        },
        showPointEventHandler: function(evt){
          var point = evt.target.cartoPoint || evt.target;
          evt.target.carto.showPoint(point);
        },
        hidePointEventHandler: function(evt){
          var point = evt.target.cartoPoint || evt.target;
          evt.target.carto.hidePoint(point);
        },
        showPointsEventHandler: function(evt){
          var carto = evt.target.carto;
          for ( var p = 0, point; p < carto.points.length; p++) {
            point = carto.points[p];
            if ( point.categories.includes(evt.target.categoryId) ) {
              carto.showPoint(point);
            }
          }
        },
        hidePointsEventHandler: function(evt){
          var carto = evt.target.carto;
          for ( var p = 0, point; p < carto.points.length; p++) {
            point = carto.points[p];
            if ( point.categories.includes(evt.target.categoryId) ) {
              carto.hidePoint(point);
            }
          }
        }
      }

      for (var i = 0, point, dispositif; i < carto.menuDispositifs.length; i++) {
        point = carto.points[i];
        if(point != undefined) {
          dispositif = carto.menuDispositifs[i];

          point.addEventListener('mousemove', carto.showPointEventHandler);
          point.addEventListener('mouseout', carto.hidePointEventHandler);
          point.addEventListener('click', function(evt) {
            window.top.location.href = evt.target.getAttributeNS(null, "data-href");
          });
          point.setAttributeNS(null, 'data-href', dispositif.attributes.href.nodeValue);
          point.carto = carto;
          point.tooltip = carto.tooltips[i];
          point.categories= point.getAttributeNS(null, 'data-categories').split(',');
          
          dispositif.carto = carto;
          dispositif.cartoPoint = point;
          dispositif.addEventListener('mousemove', carto.showPointEventHandler);
          dispositif.addEventListener('mouseout', carto.hidePointEventHandler);
          dispositif.addEventListener('focus', carto.showPointEventHandler, true);
          dispositif.addEventListener('blur', carto.hidePointEventHandler, true);
        }
      }

      // for (var i = 0, disposition; i < carto.menuDispositions.length; i++) {
      //   disposition = carto.menuDispositions[i];
      //   disposition.carto = carto;
      //   disposition.categoryId= disposition.getAttributeNS(null, "data-category-id");
      //   disposition.addEventListener('mousemove', carto.showPointsEventHandler);
      //   disposition.addEventListener('mouseout', carto.hidePointsEventHandler);
      //   disposition.addEventListener('focus', carto.showPointsEventHandler, true);
      //   disposition.addEventListener('blur', carto.hidePointsEventHandler, true);
      // }

    })();

