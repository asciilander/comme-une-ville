<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package commeuneville
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
  <div id="page" class="site">
    <nav id="accessibility-menu" role="navigation" aria-label="Menu secondaire d'accès rapide">
      <ul>
        <li>
          <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'commeuneville' ); ?></a>
        </li>
        <li>
          <a class="skip-link screen-reader-text" href="#site-navigation">Aller au menu</a>
        </li>
        <li>
          <a class="skip-link screen-reader-text" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="Accueil">Aller à l'accueil</a>
        </li>
      </ul>
    </nav>

    <header id="masthead" class="site-header">
      <div class="site-branding">
        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="Accueil"><?php bloginfo( 'name' ); ?></a></h1>
        <?php $commeuneville_description = get_bloginfo( 'description', 'display' ); ?>
        <p class="site-description"><?php echo $commeuneville_description; ?></p>
      </div><!-- .site-branding -->

      <nav id="site-navigation" class="main-navigation">
        <div class="nav-group">
        <?php
        // wp_nav_menu( array(
        //   'theme_location' => 'menu-1',
        //   'menu_id'        => 'primary-menu',
        //   'menu_class'     => 'nav-primary',
        // ) );
        ?>
        </div>
        <div class="nav-group">
          <span class="nav-title">Collectifs</span>
        <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-2',
          'menu_id'        => 'dispositifs-menu',
          'menu_class'     => 'nav nav--2',
          'link_class'     => 'nav__link--dispositifs'
        ) );
        ?>
        </div>
        <div class="nav-group">
          <span class="nav-title">Méthodes</span>
        <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-3',
          'menu_id'        => 'dispositions-menu',
          'menu_class'     => 'nav nav--3',
          'link_class'     => 'nav__link--dispositions'
        ) );
        ?>
      </nav><!-- #site-navigation -->
      <?php
        /**
         * Include carto module
         */
        require get_template_directory() . '/inc/carto.php';
        ?>
      <script type="text/javascript">
      <?php
        /**
         * Include carto js module
         */
        require get_template_directory() . '/js/carto.js';
        ?>
      </script>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
