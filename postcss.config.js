module.exports = ctx => ({
  map: ctx.options.map,
  parser: ctx.options.parser,
  plugins: {
    'postcss-easy-import': { 
      'prefix': '_',
      'extensions': ['.css']
    },
    'postcss-extend': {},
    'postcss-url': [
      { filter: '**/fonts/*.*', url: 'copy', assetsPath: 'fonts', useHash: true },
      { filter: '**/img/*.*', url: 'copy', useHash: false },
      { filter: '**/inline/*.svg', url: 'inline' }
    ],
    'postcss-preset-env': {
      'stage': 1,
      'browsers': 'last 2 versions'
    },
    'cssnano': ctx.env === 'production' ? {preset: 'default'} : false
  }
})

