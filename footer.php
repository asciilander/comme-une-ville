<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package commeuneville
 */

?>

    </div><!-- #content -->
    <?php
      get_sidebar();
    ?>
    <div class="partenaires">
      <img class="partenaires__logo" src="<?php echo get_theme_file_uri('/assets/img/logo-creatic-608.png'); ?>" width=180 alt="">
      <img class="partenaires__logo" src="<?php echo get_theme_file_uri('/assets/img/logo-experice-608.png'); ?>" width=180 alt="">
      <img class="partenaires__logo" src="<?php echo get_theme_file_uri('/assets/img/logo-paris-lumieres-608.png'); ?>" width=180 alt="">
      <img class="partenaires__logo" src="<?php echo get_theme_file_uri('/assets/img/logo-paris8-608.png'); ?>" width=180 alt="">
    </div>
    <footer id="colophon" class="site-footer">
      <div class="site-info">
        Propulsé par 
        <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'commeuneville' ) ); ?>">
          WordPress
        </a>
        <span class="sep"> | </span>
          <a href="https://framagit.org/asciilander/comme-une-ville" target="_blank">Thème <em>"Comme Une Ville"</em></a>
          par   
          <a href="http://www.cultivateurdeprecedents.org/" target="_blank">Benjamin Roux</a>
           &amp; 
          <a href="https://www.asciiland.net" target="_blank">Asciilander</a>
        <span class="sep"> | </span>
        <a href="<?php echo esc_url( admin_url() ); ?>">Admin</a>
      </div><!-- .site-info -->
    </footer><!-- #colophon -->
  </div><!-- #page -->

  <?php wp_footer(); ?>
  </div> <!-- .wrapper -->
</body>
</html>
