��          �      �        
   	       =   )    g  _   p     �  !   �  %        -     <     D     Q     Y     i     u  )   �     �  U   �          %     <     N  p  e  
   �     �  F      B  G  c   �     �  "   	     1	     I	  	   [	     e	     t	     �	  
   �	     �	  5   �	     �	  &   �	     
     .
     1
     8
                                                                  
          	                            Automattic Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> Hi. I'm a starter theme called <code>_s</code>, or <em>underscores</em>, if you like. I'm a theme meant for hacking so don't use me as a <em>Parent Theme</em>. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for. It looks like nothing was found at this location. Maybe try one of the links below or a search? Most Used Categories One thought on &ldquo;%1$s&rdquo; Oops! That page can&rsquo;t be found. Posted in %1$s Primary Primary Menu Sidebar Skip to content Tagged %1$s Theme: %1$s by %2$s. Try looking in the monthly archives. %1$s _s comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; https://underscores.me/ list item separator,  post authorby %s post datePosted on %s Project-Id-Version: _s 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/tags/_s
POT-Creation-Date: 2016-12-23 16:00+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-10-18 14:03+0200
Language-Team: 
X-Generator: Poedit 2.2
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 Automattic Les commentaires sont fermés. Poursuivre la lecture <span class="screen-reader-text”> "%s" </span> Salut. Je suis un thème de démarrage appelé <code>_s</code>, ou <em>underscores</em> si vous préférez. Je suis conçu pour être bidouillé, donc ne vous servez pas de moi comme <em>Thème Parent</em>. Tentez plutôt de faire de moi le plus beau thème WordPress de l’année&nbsp;! C’est pour ça que je suis là. Il semble qu'il n'y ait rien par ici, essayez peut-être un des liens ci-dessous ou une recherche ? Catégories les plus utilisées Une pensée sur &ldquo;%1$s&rdquo; Oops! Page introuvable. Publié dans %1$s Principal Menu principal Barre latérale Aller au contenu Tags: %1$s Theme: %1$s par %2$s. Tentez de regarder dans les archives mensuelles. %1$s _s %1$s réaction sur &ldquo;%2$s&rdquo;  https://underscores.me/ ,  par %s Publié le %s 